package com.rambo.rmi.demo.impl;

import com.rambo.rmi.demo.RmiDemoService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.rmi.Naming;

/**
 * RmiDemoServiceImpl Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>���� 17, 2018</pre>
 */
public class RmiClientlTest {

    @Before
    public void before() throws Exception {
    }

    @After
    public void after() throws Exception {
    }

    @Test
    public void testGetUserName1() throws Exception {
        String name = "rmi.service.DemoService";
        /***************** 以下为查找服务方法一 ************/
        // 获取注册表
        //Registry registry = LocateRegistry.getRegistry("localhost", 1099);
        // 查找对应的服务
        //RmiDemoService service = (RmiDemoService) registry.lookup(name);

        /***************** 以下为查找服务方法二 ************/
        RmiDemoService service = (RmiDemoService) Naming.lookup(name);

        /***************** 以下为查找服务方法三 ************/
        //Context namingContext = new InitialContext();
        //RmiDemoService service = (RmiDemoService) namingContext.lookup("rmi:" + name);

        // 调用服务
        System.out.println(service.getUserName("Jack"));
    }
}
