package com.rambo.rmi.demo.impl;

import com.rambo.httpinvoker.api.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * httpinvoker 测试
 *
 * @author Rambo 2018-09-27
 **/
public class HttpInvokerClientTest {

    private static ApplicationContext applicationContext;

    @SuppressWarnings("unchecked")
    private <T> T getSpringBeanById(String beanId) {
        if (applicationContext == null) {
            applicationContext = new ClassPathXmlApplicationContext("spring-httpinvoke-client.xml");
        }
        return (T) applicationContext.getBean(beanId);
    }

    @Test
    public void testListZxyy() throws Exception {
        UserService userService = getSpringBeanById("userService");
        System.out.println(userService.getUserById("789"));
    }
}