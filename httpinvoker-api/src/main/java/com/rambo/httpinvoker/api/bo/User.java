package com.rambo.httpinvoker.api.bo;

import java.io.Serializable;

/**
 * 用户对象封装
 *
 * @author Rambo 2018-09-20
 **/
public class User implements Serializable {
    private String uuid;
    private String name;
    private String passwd;
    private String sex;
    private String phone;
    private String photo;
    private String email;
    private String createBy;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public String toString() {
        return "User{" + "uuid='" + uuid + '\'' + ", name='" + name + '\'' + ", passwd='" + passwd + '\'' + ", sex='" + sex + '\'' + ", phone='" + phone + '\'' + ", photo='" + photo + '\'' + ", email='" + email + '\'' + ", createBy='" + createBy + '\'' + '}';
    }
}